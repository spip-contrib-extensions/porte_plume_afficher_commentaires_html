<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-commhtml
// Langue: fr
// Date: 26-06-2012 17:03:10
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// C
	'commhtml_description' => 'Ce plugin permet d\'afficher dans l\'espace privé les commentaires HTML saisis dans le texte des objets éditoriaux (les articles par exemple). Enfin, un bouton est ajouté au porte-plume pour insérer facilement un commentaire.',
	'commhtml_slogan' => 'Affiche les commentaires HTML dans l\'espace privé',
);
?>